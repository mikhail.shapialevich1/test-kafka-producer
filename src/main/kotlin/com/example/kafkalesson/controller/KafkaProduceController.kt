package com.example.kafkalesson.controller

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import uk.co.pnh.ecom.search.model.ActionType
import uk.co.pnh.ecom.search.model.Attribute
import uk.co.pnh.ecom.search.model.AttributeType
import uk.co.pnh.ecom.search.model.Product
import uk.co.pnh.ecom.search.model.ProductKey
import java.util.*
import kotlin.random.Random

@RestController
@RequestMapping("/api/kafka")
class KafkaProduceController(
    private val kafkaTemplate: KafkaTemplate<String, String>,
    private val objectMapper: ObjectMapper,
    private val avroKafkaTemplate: KafkaTemplate<ProductKey, Product>
) {

    @GetMapping("/{count}")
    fun produceRecords(@PathVariable count: Int) {
        for (i in 0..count) {
            kafkaTemplate.send("product-index-data", getMessage())
        }
    }

    @GetMapping("/one")
    fun produceRecord() {
        kafkaTemplate.send("product-index-data", getOneMessage())
    }

    @GetMapping("/delete")
    fun deleteRecord() {
        kafkaTemplate.send("product-index-data", deleteMassage())
    }

    @GetMapping("/product")
    fun sendProductRecord() {
        kafkaTemplate.send("product-index-data", getProductMessage())
    }

    @GetMapping("/avro")
    fun sendAvroProduct() {
        val product = getAvroMessage()
        avroKafkaTemplate.send("product-index-data", ProductKey(product.objectId, product.objectId), product)
    }

    private fun getMessage(): String {
        val source = """
    {
        "first": 2.89, 
        "second": {
            "a": "aaaaa",
            "b": "bbbb",
            "c": [true, false, true]
            },
        "third": true,
        "fourth": "simple string",
        "fifth": [1, 2, 3, 4],
        "six": [
            {
                "name": "Misha",
                "phoneNumber": 123456789,
                "busy": false
            },
            {
                "name": "Oleksey",
                "phoneNumber": 987654321,
                "busy": true
            }
        ],
        "seven": [true, false, true, true]
    }
        """.trimIndent()
        val actionType = if (Random.nextBoolean()) ActionType.SAVE else ActionType.DELETE
        val product = Product(
            UUID.randomUUID().toString(),
            actionType,
            listOf(
                Attribute("first", AttributeType.STRING, "random value"),
                Attribute("second", AttributeType.LIST, "[\"1\", \"2\", \"3\"]"),
                Attribute("third", AttributeType.NUMBER, "2.89"),
                Attribute("fourth", AttributeType.BOOLEAN, "true"),
                Attribute("fifth", AttributeType.JSON, source)
            )
        )
        return objectMapper.writeValueAsString(product)
    }

    private fun getOneMessage(): String {
        val source = """
    {
        "first": 4.56, 
        "second": {
            "a": "bbb",
            "b": "ccc",
            "c": [true, false, true]
            },
        "third": true,
        "fourth": "simple string",
        "fifth": [1, 2, 3, 4]
    }
        """.trimIndent()
        val product = Product(
            "d95eaf6f-ffac-4ce4-ba54-04270b9059dd",
            ActionType.SAVE,
            listOf(
//                Attribute("first", AttributeType.STRING, "replaced value"),
//                Attribute("second", AttributeType.LIST, "[\"5\", \"6\", \"7\"]"),
//                Attribute("third", AttributeType.NUMBER, "3.15"),
//                Attribute("fourth", AttributeType.BOOLEAN, "true"),
//                Attribute("fifth", AttributeType.JSON, source)
            )
        )
        return product.toString()
    }

    private fun getAvroMessage(): Product {
        val source = """
    {
        "first": 4.56, 
        "second": {
            "a": "bbb",
            "b": "ccc",
            "c": [true, false, true]
            },
        "third": true,
        "fourth": "simple string",
        "fifth": [1, 2, 3, 4]
    }
        """.trimIndent()
        val product = Product(
            "catalog2:" + UUID.randomUUID().toString(),
            ActionType.SAVE,
            listOf(
                Attribute("first", AttributeType.STRING, "replaced value"),
                Attribute("second", AttributeType.LIST, "[\"5\", \"6\", \"7\"]"),
                Attribute("third", AttributeType.NUMBER, "3.15"),
                Attribute("fourth", AttributeType.BOOLEAN, "true"),
                Attribute("fifth", AttributeType.JSON, source)
            )
        )
        return product
    }

    private fun deleteMassage(): String {
        val product = Product(
            "d95eaf6f-ffac-4ce4-ba54-04270b9059dd",
            ActionType.DELETE,
            listOf(
                Attribute("first", AttributeType.STRING, "replaced value"),
                Attribute("second", AttributeType.LIST, "[\"5\", \"6\", \"7\"]"),
                Attribute("third", AttributeType.NUMBER, "3.15"),
                Attribute("fourth", AttributeType.BOOLEAN, "true")
            )
        )
        return objectMapper.writeValueAsString(product)
    }

    private fun getProductMessage(): String {
        val product = """
            {
  "objectId": "555952:cpsWebProductCatalog",
  "action": "SAVE",
  "attributes": [
    {
      "name": "Availability_string",
      "type": "STRING",
      "value": "Collection Only"
    },
    {
      "name": "Breaker Class_string_mv",
      "type": "STRING",
      "value": "Vaillant"
    },
    {
      "name": "Guarantee_string_mv",
      "type": "STRING",
      "value": "7"
    },
    {
      "name": "Range Description_string_mv",
      "type": "STRING",
      "value": "Xtreme"
    },
    {
      "name": "Years Guaranteed_string_mv",
      "type": "STRING",
      "value": "7"
    },
    {
      "name": "minimumOrderQtyValueProvider_int",
      "type": "NUMBER",
      "value": "0"
    },
    {
      "name": "ucp_en_boolean",
      "type": "BOOLEAN",
      "value": "false"
    },
    {
      "name": "name_text_en",
      "type": "STRING",
      "value": "7years IGAS XTREME 24 COMBI BOILERCPS.Only.Tst.302CG150419"
    },
    {
      "name": "ean",
      "type": "STRING",
      "value": "1100746007"
    },
    {
      "name": "img-normal",
      "type": "STRING",
      "value": "//dam-uat.k8test.cityplumbing.co.uk/oz6814m/GPID_1100746007_IMG_01.jpg?width=300&height=300"
    },
    {
      "name": "img-thumbnail",
      "type": "STRING",
      "value": "//dam-uat.k8test.cityplumbing.co.uk/oz6814m/GPID_1100746007_IMG_01.jpg?width=96&height=96"
    },
    {
      "name": "featuresAndBenefits",
      "type": "LIST",
      "value": "[\"PFGHR unit\",\"Boiler Plus Ready\",\"Touch screen display\",\"11-15LPM flow rate\"]"
    },
    {
      "name": "sellOnly",
      "type": "BOOLEAN",
      "value": "false"
    },
    {
      "name": "uom",
      "type": "STRING",
      "value": "EA"
    },
    {
      "name": "categoryName_string_mv",
      "type": "LIST",
      "value": "[\"1838005\"]"
    },
    {
      "name": "description_text_en",
      "type": "STRING",
      "value": "IGAS XTREME 24 COMBI BOILER CG150419 CPS.Only.Tst.302"
    },
    {
      "name": "Brand_en_string_mv",
      "type": "STRING",
      "value": "Vaillant"
    },
    {
      "name": "code_string",
      "type": "STRING",
      "value": "619877"
    },
    {
      "name": "supplierPartNumber_string_mv",
      "type": "STRING",
      "value": "CPS.Only.Tst.302"
    },
    {
      "name": "objectID",
      "type": "STRING",
      "value": "1100746007"
    }
  ]
}
        """.trimIndent()
        return product
    }
}
package com.example.kafkalesson.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.integration.transformer.GenericTransformer
import org.springframework.stereotype.Component
import uk.co.pnh.ecom.search.model.Product

@Component
class JsonToProductTransformer(
    private val objectMapper: ObjectMapper
) : GenericTransformer<String, Product> {
    override fun transform(source: String): Product {
        return objectMapper.readValue(source)
    }
}